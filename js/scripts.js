$(document).ready(function() {
  $(".clickable").click(function() {
    $(".initially-hidden").fadeToggle();
    $(".initially-showing").fadeToggle();
  });
});

$(function() {
  $("li").click(function() {
    $(".list").slideUp();
  });
});
